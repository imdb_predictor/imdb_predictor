# Predicting IMDb Popularity #
In this work, we focus on content rather than collaborative signals and investigate whether the core movie data provides enough signals to predict whether a given movie will be popular.  More specifically, we propose to apply supervised learning techniques, in order to predict whether any given movie will be a “blockbuster” and, which rating, in the IMDb star scale, it might obtain. Note that the task is intrinsically challenging, given that even popular review sites like IMDb might paint a distorted picture. Yet we will show that a decent, if not perfect, level of accuracy can be achieved by comparing our results  to test data from the past.

## Prerequisites ##
* Python 2.7
* numpy
* scipy
* scikit-learn

## Configuration ##
The latest code is located under new_src folder, where Model.py runs the project.