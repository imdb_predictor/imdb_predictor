import csv
import os

__author__ = 'czarfati'


class Utils:
    """
    This class contains all utilities methods for the pre-processing part
        create_main_db_file

    """
    db_schema = [  "id",
                   "movie_title",
                   "cast",  # list of tuples of (id, name, rank)
                   "cast_rate",  # weighted average rate
                   "crew",  # list of tuples of (id, name, rank)
                   "crew_rate",  # weighted average rate
                   "producers",  # list of tuples of (id, name, rank)
                   "producers_rank",  # weighted average rate
                   "directors",
                   "directors_rank",
                   "genre",
                   "year",
                   "origin_country",
                   "language",
                   "budget",
                   "imdb_rank",
                   "classification",
                   ]

    @staticmethod
    def print_debug(msg, flag):
        debug_indication = "[ "
        if flag == "i":
            debug_indication += "INFO ]"
        elif flag == "d":
            debug_indication += "DEBUG ]"
        elif flag == "e":
            debug_indication += "ERROR ]"
        print(debug_indication + " [*** PRE-PROCESSING ***]" + msg)

    @staticmethod
    def create_main_db_file(path_to_file):
        if os.path.exists(path_to_file):
            Utils.print_debug("file already exists", "i")
            Utils.print_debug("delete file and create new one", "d")
        file = open(path_to_file, "wb+")
        writer = csv.DictWriter(file, Utils.db_schema)
        writer.writeheader()


    # @staticmethod
    # def get_csv_handlers(path_to_csv, schema):
    #     if not os.path.exists(path_to_csv):
    #         Utils.print_debug("file doesn't exist - creating one", "i")
    #     f_w = open(path_to_csv, "wb")
    #     f_r = open(path_to_csv, "rb")
    #     writer = csv.DictWriter(f_w, schema)
    #     reader = csv.DictReader(f_r, schema)
    #     return writer, reader

