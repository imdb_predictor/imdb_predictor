from collections import namedtuple
import csv
from gzip import GzipFile
import os

__author__ = 'czarfati'

CAST_START = ('Name', '----')
CAST_STOP = '-----------------------------'
IMDB_PTDF_DIR = os.path.normpath("c:/users/czarfati/pycharmprojects/imdb_predictor/db/raw/zipped/")
IMDB_CSV_DIR = os.path.normpath("c:/users/czarfati/pycharmprojects/imdb_predictor/db/raw/csv/")
CastMember = namedtuple('CastMember', ['id', 'movie_list', ])
CastMemberMovieEntry = namedtuple('CastMemberMovieEntry', ['id', 'name', 'note', 'order', 'role_list'])


def getSectionHash(fp):
    """Return sections separated by lines starting with #."""
    curSectList = []
    curSectListApp = curSectList.append
    curTitle = ''
    joiner = ''.join
    for line in fp:
        if line and line[0] == '#':
            if curSectList and curTitle:
                yield curTitle, joiner(curSectList)
                curSectList[:] = []
                curTitle = ''
            curTitle = line[2:]
        else:
            curSectListApp(line)
    if curSectList and curTitle:
        yield curTitle, joiner(curSectList)
        curSectList[:] = []
        curTitle = ''


NMMVSections = dict([(x, None) for x in ('MV: ', 'NM: ', 'OT: ', 'MOVI')])


def getSectionNMMV(fp):
    """Return sections separated by lines starting with 'NM: ', 'MV: ',
    'OT: ' or 'MOVI'."""
    curSectList = []
    curSectListApp = curSectList.append
    curNMMV = ''
    joiner = ''.join
    for line in fp:
        if line[:4] in NMMVSections:
            if curSectList and curNMMV:
                yield curNMMV, joiner(curSectList)
                curSectList[:] = []
                curNMMV = ''
            if line[:4] == 'MOVI':
                curNMMV = line[6:]
            else:
                curNMMV = line[4:]
        elif not (line and line[0] == '-'):
            curSectListApp(line)
    if curSectList and curNMMV:
        yield curNMMV, joiner(curSectList)
        curSectList[:] = []
        curNMMV = ''


GzipFileRL = GzipFile.readline


class SourceFile(GzipFile):
    """Instances of this class are used to read gzipped files,
    starting from a defined line to a (optionally) given end."""

    def __init__(self, filename=None, mode=None, start=(), stop=None,
                 pwarning=1, *args, **kwds):
        filename = os.path.join(IMDB_PTDF_DIR, filename)
        try:
            GzipFile.__init__(self, filename, mode, *args, **kwds)
        except IOError, e:
            if not pwarning: raise
            print 'WARNING WARNING WARNING'
            print 'WARNING unable to read the "%s" file.' % filename
            print 'WARNING The file will be skipped, and the contained'
            print 'WARNING information will NOT be stored in the database.'
            print 'WARNING Complete error: ', e
            # re-raise the exception.
            raise
        self.start = start
        for item in start:
            itemlen = len(item)
            for line in self:
                if line[:itemlen] == item: break
        self.set_stop(stop)

    def set_stop(self, stop):
        if stop is not None:
            self.stop = stop
            self.stoplen = len(self.stop)
            self.readline = self.readline_checkEnd
        else:
            self.readline = self.readline_NOcheckEnd

    def readline_NOcheckEnd(self, size=-1):
        line = GzipFile.readline(self, size)
        return unicode(line, 'latin_1').encode('utf_8')

    def readline_checkEnd(self, size=-1):
        line = GzipFile.readline(self, size)
        if self.stop is not None and line[:self.stoplen] == self.stop: return ''
        return unicode(line, 'latin_1').encode('utf_8')

    def getByHashSections(self):
        return getSectionHash(self)

    def getByNMMVSections(self):
        return getSectionNMMV(self)


# TODO this is how we should use the supplied utilities methods
# try:
#     f = SourceFile(fname, start=CAST_START, stop=CAST_STOP)
# except IOError:
# doCast(f, roleid, rolename)



def printDebug(flag, msg):
    pre_msg = "[ "
    if flag == "e":
        pre_msg += "ERROR ]"
    elif flag == "d":
        pre_msg += "DEBUG ]"
    else:
        pre_msg += "INFO ]"
    print(pre_msg + ":\t" + msg)


def getMovieId(title):
    return 0


def doCast(fp, rolename):
    """Populate the cast table."""
    count = 0
    pid = count
    name = ''

    csvfile = open(IMDB_CSV_DIR + "\\cast\\" + rolename + "s" + ".csv", "w")
    csv_fields = ["id", "name", "gender", "movieid", "movietitle", "roles", "note", "order"]
    writer = csv.DictWriter(csvfile, fieldnames=csv_fields)
    writer.writeheader()

    for line in fp:
        # TODO 's
        # STEP 1: handle new entries
        # STEP 2: parse and create details dictionary for each new movie entry for that actor/ess
        # STEP 3: write it to new file

        if line and line[0] != '\t':
            if line[0] == '\n': continue
            sl = filter(None, line.split('\t'))
            if len(sl) != 2: continue
            name, line = sl
            miscData = None
            if rolename == 'actor':
                miscData = [('personGender', 'm')]
            elif rolename == 'actress':
                miscData = [('personGender', 'f')]
            pid = count
            count += 1

        line = line.strip()
        ll = line.split('  ')
        movieid, note, order, role, title = parseCastLine(ll)
        if movieid is None:
            printDebug("e",
                       "The movie: ---{0}--- , "
                       "is not on movies DB, therefore it isn't listed in the actor's record".format([title]))
            continue
        roles = []
        if role is not None:
            roles = filter(None, [x.strip() for x in role.split('/')])
            # for role in roles:
            # cid = CACHE_CID.addUnique(role)
            # sqldata.add((pid, movieid, cid, note, order))
        else:
            printDebug("i", "Roles not mentioned for actor {0}".format(name))

        writer.writerow(
            {"id": pid, "name": name, "gender": miscData, "movieid": movieid, "movietitle": title, "roles": roles,
             "note": note, "order": order})
        if count % 10000 == 0:
            printDebug("i", "inserting line to csv file: id-{0} | name-{1} | gender-{7} | movieid-{2} | "
                            "movietitle-{3} | roles-{4} | note-{5} | order-{6}".format(pid, name, movieid, title, roles,
                                                                                       note, order, miscData))


def parseCastLine(ll):
    note = None
    role = None
    order = None
    title = ll[0]  # movie/episode title
    for item in ll[1:]:
        if not item: continue
        if item[0] == '[':
            # Quite inefficient, but there are some very strange
            # cases of garbage in the plain text data files to handle...
            role = item[1:]
            if role[-1:] == ']':
                role = role[:-1]
            if role[-1:] == ')':
                nidx = role.find('(')
                if nidx != -1:
                    note = role[nidx:]
                    role = role[:nidx].rstrip()
                    if not role: role = None
        elif item[0] == '(':
            if note is None:
                note = item
            else:
                note = '%s %s' % (note, item)
        elif item[0] == '<':
            textor = item[1:-1]
            try:
                order = long(textor)
            except ValueError:
                os = textor.split(',')
                if len(os) == 3:
                    try:
                        order = ((long(os[2]) - 1) * 1000) + \
                                ((long(os[1]) - 1) * 100) + (long(os[0]) - 1)
                    except ValueError:
                        pass
    movieid = getMovieId(title)  # TODO implement after UZI finish movie dict
    return movieid, note, order, role, title


def _main():
    # TODO 's:
    # STEP 1: Iterate actors+actress .gz files
    # STEP 2: Put into files
    # STEP 3: Concatenate into one file
    ACTORS_RAW = 'actors.list.gz'
    ACTRESS_RAW = 'actress.list.gz'
    ACTOR = 'actor'
    ACTRESS = 'actress'
    file_names = [ACTORS_RAW, ACTRESS_RAW]
    for fname in file_names:
        try:
            f = SourceFile(fname, start=CAST_START, stop=CAST_STOP)
        except IOError:
            print '[ ERROR ]\tI/O exception during SourceFile ctor'
        doCast(f, 'actor')


if __name__ == '__main__':
    _main()
