import csv


def is_english(title):
    try:
        title.decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def create_dict(flag):
    test_flag = False  # test flag will only do 20 iterations and print result
    # flag: True = create movie name dict, False = create years dict
    i = 0  # used for testing
    list_of_movies = []
    list_of_years = []
    path = "C:\Smuzi\Projects\imdb_predictor\src\preprocessing\movies.list"
    prev_title = ''
    with open(path) as f:
        file_lines = f.readlines()

    for single_line in file_lines:
        if (i < 20 and test_flag) or not test_flag:
            # check if it really is a movie, and remove special titles:
            if '(V)' not in single_line and '(TV)' not in single_line and '(VG)' not in single_line \
                    and '(????)' not in single_line and '{{SUSPENDED}}' not in single_line and '/I' not in single_line \
                    and '/V' not in single_line and '/X' not in single_line:
                tmp = single_line.strip().split("(")
                count = single_line.count('(')
                if count == 1:
                    title = tmp[0].strip()
                    year = tmp[1].split("\t")[0].rstrip(')')

                else:
                    title = tmp[0]
                    for j in range(1, count):
                        title = title + '(' + tmp[j]
                    year = tmp[count].split("\t")[0].rstrip(')')
                # only english characters:
                if is_english(title):
                    # if title not in list_of_movies:
                    if title.lower() != prev_title.lower():
                        list_of_years.append(year)
                        list_of_movies.append(title)
                        i += 1  # only for testing
                        prev_title = title
    # print len(list_of_movies)
    # exit()
    if test_flag:
        print list_of_movies
    # print to csv:
    if flag:
        with open('movie_dict.csv', 'wb') as f:
            wr = csv.writer(f, lineterminator='\n')
            for movie in list_of_movies:
                wr.writerow([movie])
    if not flag:
        with open('movie_data.csv', 'wb') as f:
            wr = csv.writer(f, lineterminator='\n')
            i = 0
            for year in list_of_years:
                wr.writerow([i, year])
                i += 1

    # to read the csv:
    # with open('movie_dict.csv', 'rb') as f:
    #    reader = csv.reader(f)
    #    list1 = list(reader)
    #    print list1


def collect_ratings():
    test_flag = False
    path = "C:\\Smuzi\\Projects\\imdb_predictor\\src\\preprocessing\\ratings.list"

    # get movies list:
    with open('movie_dict.csv', 'rb') as f:
        reader = csv.reader(f)
        movies = list(reader)
    movie_dict = dict(enumerate(movies))
    movie_dict_r = dict((v[0], k) for k, v in movie_dict.iteritems())  # reverse key index(find id by movie)

    with open('movie_data.csv', 'rb') as f:
        reader = csv.reader(f)
        movie_years = dict(reader)
    #print movie_years[str(0)]


    # create rating dict:
    rating_dict = dict.fromkeys(range(0, len(movie_dict)-1), 0.0)
    with open(path) as f:
        file_lines = f.readlines()
    i = 0
    count = 0
    for single_line in file_lines:
        if (i < 20 and test_flag) or not test_flag:
            split_line = single_line.strip().split(" ")
            i += 1
            # extract rating:
            rating_index = 0
            for j in range(0, len(split_line)):
                temp = split_line[j]
                if len(temp) == 3:
                    if '.' in temp:
                        rating = temp
                        rating_index = j
                        break
            # rating is isolated. now need to match to movie_id
            # first i will get the movie name and from that i will get the id:
            title = ''
            for k in range(rating_index+2, len(split_line)-1):
                if k == rating_index+2:
                    title = split_line[k]
                    #print split_line[k]
                else:
                    title = title + ' ' + split_line[k]
            year = split_line[len(split_line)-1].rstrip(')').lstrip('(')
            if title in movie_dict_r:

                id = movie_dict_r[title]
                if year == movie_years[str(id)]:
                    # we got the right movie name corresponding the right year:
                    rating_dict[id] = rating
                    count += 1
    print count

    with open('movie_data.csv', 'rb') as finput:
        with open('movie_data_2.csv', 'wb') as foutput:
            writer = csv.writer(foutput, lineterminator='\n')
            reader = csv.reader(finput)
            # new_lines = []
            i=0
            for row in reader:
                new_row = [row[0], row[1], str(rating_dict[int(row[0])])]
                if rating_dict[int(row[0])] != 0:
                    i += 1
                writer.writerow(new_row)
    print i
#current size: 655703
if __name__== "__main__":
    # create_dict(True)
    # create_dict(False)
    collect_ratings()
    #with open('movie_dict.csv', 'rb') as f:
    #    reader = csv.reader(f)
    #    movies = list(reader)[:10]
    #print dict(enumerate(movies))