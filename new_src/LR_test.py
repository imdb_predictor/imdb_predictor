from sklearn import linear_model

from Utils import *
# import sklearn.ensemble.RandomForestClassifier
# import sklearn.neighbors.KNeighborsClassifier
# import sklearn.tree.DecisionTreeClassifier

titles = ['Logistic Regression parameter testing']

ACCEPTED_ACCURACY = 0.76

def runModel1():
    X_train, X_test, y_train, y_test = create_x_y()
    print titles[0]

    penalty_p = ['l2']
    dual_p = [True, False]
    c_p = [0.5, 1.0]
    fit_p = [True, False]
    inter_p = [0.1, 0.5, 1.0, 1.5]
    iter_p = [50, 100, 150]
    solver_p = ['newton-cg', 'lbfgs', 'liblinear']
    tol_p = [0.0001]

    for pen in penalty_p:
        for dual in dual_p:
            for c in c_p:
                for fit in fit_p:
                    for inter in inter_p:
                        for i in iter_p:
                            for solver in solver_p:
                                for tol in tol_p:
                                    lr_model = linear_model.LogisticRegression(penalty=pen, dual=dual, tol=tol, C=c,
                                                fit_intercept=fit, intercept_scaling=inter,
                                                solver=solver).fit(X_train, y_train.ravel())
                                    score = lr_model.score(X_test, y_test.ravel())
                                    if score > ACCEPTED_ACCURACY:
                                        print("penalty:" + pen + " iter:" + str(i) + " dual:" + str(dual) + " c:" +
                                              str(c) + " fit:" + str(fit) + " inter:" + str(inter) + " solver:" +
                                              str(solver) + " tolerance:" + str(tol))
                                        print("Accuracy: %0.4f%% " % (score))
                                        print("---")

# best result:
# accuracy: 0.7622
# nondefault: c=1, inter=0.5


if __name__ == "__main__":
    runModel1()
    print("done")

