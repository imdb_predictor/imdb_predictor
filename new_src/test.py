import numpy as np
# all categories:
# movie_title, color, num_critic_for_reviews, movie_facebook_likes, duration, director_name
# director_facebook_likes, actor_3_name, actor_3_facebook_likes, actor_2_name,actor_2_facebook_likes
# actor_1_name, actor_1_facebook_likes, gross, genres, num_voted_users, cast_total_facebook_likes
# facenumber_in_poster, plot_keywords, movie_imdb_link, num_user_for_reviews, language, country, content_rating
# budget, title_year, imdb_score, aspect_ratio


# genres,language,country,
# def model_categories()


# category: 0=genres,1=language,2=country
def model_single_category(col, category):
    size= len(col)
    if category == 0:  # genre
        all_genres = get_all_genres(col)
        sorted_genres = list(all_genres)
        num_of_genres = len(sorted_genres)
        sorted_genres.sort()

        new_col = np.zeros((size, num_of_genres))
        index = 0
        for single_val in col:
            for single_genre in single_val.split("|").strip():
                genre_index = sorted_genres.index(single_genre)
                new_col[index][genre_index] = 1
            index += 1
    elif category == 1:  # language
        new_col = np.zeros(size)
        index = 0
        for single_val in col:
            is_english = False
            if single_val == "English":
                is_english = True
            new_col[index] = is_english
            index += 1
    elif category == 2:  # country
        new_col = np.zeros(size)
        index = 0
        for single_val in col:
            is_usa = False
            if single_val == "USA":
                is_usa = True
            new_col[index] = is_usa
            index += 1

def get_all_genres(col):
    all_genres = set()
    for item in col:
        movie_genres = item.split("|").strip()
        for genre in movie_genres:
            if not genre in all_genres:
                all_genres.add(genre)
    return all_genres

if __name__== "__main__":
    # dosomthing
    a = 1
    b = a
    a = b

