import csv
import numpy as np
from sklearn import neighbors, ensemble, svm, tree, linear_model
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, Imputer

RAW_CSV_FEATURES = [
    ("color", str),
    ("director_name", str),
    ("director_facebook_likes", int),
    ("duration", int),
    ("num_critic_for_reviews", int),
    ("actor_1_name", str),
    ("actor_1_facebook_likes", int),
    ("actor_2_name", str),
    ("actor_2_facebook_likes", int),
    ("actor_3_name", str),
    ("actor_3_facebook_likes", int),
    ("gross", int),
    ("genres", [str]),
    ("movie_title", str),
    ("num_voted_users", int),
    ("language", str),
    ("cast_total_facebook_likes", int),
    ("facenumber_in_poster", int),
    ("plot_keywords", [str]),
    ("movie_imdb_link", str),
    ("num_user_for_reviews", int),
    ("country", str),
    ("content_rating", str),
    ("budget", int),
    ("title_year", int),
    ("aspect_ratio", float),
    ("movie_facebook_likes", int),
]

NUM_OF_GENRES = 24

GENRES = [
    'Sci-Fi',
    'Crime',
    'Romance',
    'Animation',
    'Music',
    'Comedy',
    'War',
    'Horror',
    'Film-Noir',
    'Western',
    'News',
    'Thriller',
    'Adventure',
    'Mystery',
    'Short',
    'Drama',
    'Action',
    'Documentary',
    'Musical',
    'History',
    'Family',
    'Fantasy',
    'Sport',
    'Biography',
]

MODEL_FEATURES = [
                     "color",
                     "duration",
                     "director_facebook_likes",
                     "actor_1_facebook_likes",
                     "actor_2_facebook_likes",
                     "actor_3_facebook_likes",
                     "language",
                     "country",
                     "budget",
                     "title_year",
                     "facenumber_in_poster",
                     "aspect_ratio",
                 ] + \
                 [feature for feature in ('is_{}_genre'.format(GENRES[genre]) for genre in xrange(len(GENRES)))]

LABEL_FIELDNAME = "imdb_score"

HIGH_SCORE_THRESHOLD = 7.5

TRAIN_TEST_RELATION = [0.85, 0.15]

raw_csv_file_path = "./movie_metadata.csv"
sorted_csv_file_path = "./movie_metadata_sorted.csv"


def print_genres_data(csv_path="./movie_metadata.csv"):
    genres = set()
    for row in csv.DictReader(open(csv_path)):
        for genre in row["genres"].split("|"):
            genres.add(genre)
    print len(genres)
    print genres


def print_debug(str, method_name="NONE"):
    print('[DEBUG][{}]:\t{}'.format(method_name, str))


def print_data_info(X_test, X_train, y_test, y_train):
    train_good_samples_count = len([sample for sample in y_train if sample == 1])
    train_bad_samples_count = X_train.shape[0] - train_good_samples_count
    print_debug("train data - num of samples: {}".format(len(X_train)), create_x_y.__name__)
    print_debug("train data - num of 'good' samples: {}".format(train_good_samples_count), create_x_y.__name__)
    print_debug("train data - num of 'bad' samples: {}".format(train_bad_samples_count), create_x_y.__name__)
    print_debug("train data - 'good' / 'bad' ratio: {0:.3f}".format(
        float(float(train_good_samples_count) / train_bad_samples_count)), create_x_y.__name__)
    test_good_samples_count = len([sample for sample in y_test if sample == 1])
    test_bad_samples_count = X_test.shape[0] - test_good_samples_count
    print_debug("test data - num of samples: {}".format(len(X_test)), create_x_y.__name__)
    print_debug("test data - num of 'good' samples: {}".format(test_good_samples_count), create_x_y.__name__)
    print_debug("test data - num of 'bad' samples: {}".format(test_bad_samples_count), create_x_y.__name__)
    print_debug("test data - 'good' / 'bad' ratio: {0:.3f}".format(
        float(float(test_good_samples_count) / test_bad_samples_count)),
        create_x_y.__name__)


def get_binary_label(score):
    res = 1 if score >= HIGH_SCORE_THRESHOLD else -1
    return res


def sort_csv_by_year(reader):
    result = sorted(reader, key=lambda d: int(d['title_year']))
    writer = csv.DictWriter(open(sorted_csv_file_path, 'wb'), reader.fieldnames)
    writer.writeheader()
    writer.writerows(result)
    return len(result), csv.DictReader(open(sorted_csv_file_path))


def get_feature_vals_from_row(row):
    color = 1 if str(row["color"]).lower() == "color" else 0 if str(
        row["color"]).lower() == " black and white" else np.nan
    duration = np.nan if row["duration"] == '' else int(row["duration"])
    dir_likes = np.nan if int(row["director_facebook_likes"]) == 0 else int(
        row["director_facebook_likes"])
    a1_likes = np.nan if row["actor_1_facebook_likes"] == '' else int(row["actor_1_facebook_likes"])
    a2_likes = np.nan if row["actor_2_facebook_likes"] == '' else int(row["actor_2_facebook_likes"])
    a3_likes = np.nan if row["actor_3_facebook_likes"] == '' else int(row["actor_3_facebook_likes"])
    language = 1 if row["language"] == "English" else 0
    country = 1 if row["country"] == "USA" else 0
    budget = np.nan if row["budget"] == '' else int(row["budget"])
    year = int(row["title_year"])
    face_num = np.nan if row["facenumber_in_poster"] == '' else int(row["facenumber_in_poster"])
    aspect_ratio = np.nan if row["aspect_ratio"] == '' else float(row["aspect_ratio"])
    genres = [1 if db_genre in row["genres"].split("|") else 0 for db_genre in GENRES]
    feature_vector = [color, duration, dir_likes, a1_likes, a2_likes, a3_likes, language, country, budget,
                      year, face_num, aspect_ratio] + genres
    return feature_vector


def scale_and_split(X, y, use_permutations):
    X = Imputer(missing_values="NaN", strategy="median", axis=0).fit_transform(X)
    print_debug("Finished imputations using median strategy...", scale_and_split.__name__)
    X = StandardScaler().fit_transform(X)
    print_debug("Finished feature scaling using standard scaler...", scale_and_split.__name__)
    if use_permutations:
        X = PolynomialFeatures(interaction_only=False).fit_transform(X)
        print_debug("Finished feature permutation using deg 2 permutator...", scale_and_split.__name__)
        print_debug("X matrix shape - {}".format(X.shape), scale_and_split.__name__)
    split_idx = int(np.floor(TRAIN_TEST_RELATION[0] * X.shape[0]))
    X_train, X_test, y_train, y_test = X[:split_idx], X[split_idx + 1:], y[:split_idx], y[split_idx + 1:]
    return X_train, X_test, y_train, y_test


def create_x_y(csv_file_path="./movie_metadata.csv", binary=True, use_feature_permutations=False, scale_data=True):
    print_debug("Start creating X, y matrices...", create_x_y.__name__)
    rows_num, input_file = sort_csv_by_year(csv.DictReader(open(csv_file_path)))
    print_debug("Information, total examples:{}".format(rows_num), create_x_y.__name__)
    X = np.zeros((rows_num, len(MODEL_FEATURES)), dtype=float)
    y = np.zeros((rows_num, 1), dtype=int)
    for i, row in enumerate(input_file):
        X[i] = get_feature_vals_from_row(row)
        if not binary:
            y[i] = round(float(row[LABEL_FIELDNAME]))  # rounding rating to deal with 0-9 rating
        else:
            y[i] = get_binary_label(float(row[LABEL_FIELDNAME]))
    print_debug("Finished parsing raw db...", create_x_y.__name__)
    if scale_data:
        X_train, X_test, y_train, y_test = scale_and_split(X, y, use_feature_permutations)
    else:
        return X, y
    print_data_info(X_test, X_train, y_test, y_train)
    print_debug("Finished creating X, y matrices...", create_x_y.__name__)
    return X_train, X_test, y_train, y_test


def calibrate_weights_binary():
    X_train, X_test, y_train, y_test = create_x_y()

    cl_svm = svm.SVC(kernel='linear', C=2, cache_size=2000, probability=True).fit(X_train,
                                                                                  y_train.ravel())  # FIXME class_weight param
    cl_rf = ensemble.RandomForestClassifier(criterion="entropy", ).fit(X_train, y_train.ravel())
    cl_dt = tree.DecisionTreeClassifier(criterion="entropy").fit(X_train, y_train.ravel())
    cl_knn = neighbors.KNeighborsClassifier().fit(X_train, y_train.ravel())
    # cl_perc = linear_model.Perceptron().fit(X_train, y_train.ravel())
    cl_logr = linear_model.LogisticRegression().fit(X_train, y_train.ravel())

    prob_1 = cl_svm.predict_proba(X_test)

    prob_2 = cl_rf.predict_proba(X_test)
    prob_3 = cl_dt.predict_proba(X_test)
    prob_4 = cl_knn.predict_proba(X_test)
    # prob_5 = cl_perc.predict_proba(X_test)
    prob_5 = cl_logr.predict_proba(X_test)
    return find_best_weights(prob_1, prob_2, prob_3, prob_4, prob_5, y_test)


def calibrate_weights_multiclass():
    print "this test will take a while to run(expect last print to be 171000)"
    X_train, X_test, y_train, y_test = create_x_y(binary=False)

    cl_dt = tree.DecisionTreeClassifier(criterion="entropy").fit(X_train,
                                                                 y_train.ravel())  # TODO: visualization http://scikit-learn.org/stable/modules/tree.html
    cl_knn = neighbors.KNeighborsClassifier(n_neighbors=50, weights='distance').fit(X_train, y_train.ravel())
    cl_logr = linear_model.LogisticRegression(solver='lbfgs', multi_class='multinomial').fit(X_train, y_train.ravel())
    cl_rf = ensemble.RandomForestClassifier(criterion="entropy", ).fit(X_train, y_train.ravel())

    prob_1 = cl_dt.predict_proba(X_test)
    prob_2 = cl_knn.predict_proba(X_test)
    prob_3 = cl_logr.predict_proba(X_test)
    prob_4 = cl_rf.predict_proba(X_test)
    for c in [cl_dt, cl_knn, cl_logr, cl_rf]:
        print c.score(X_test, y_test.ravel())
    return find_best_weights_multiclass(prob_1, prob_2, prob_3, prob_4, y_test)


def find_best_weights(prob_1, prob_2, prob_3, prob_4, prob_5, y_test):
    count = 0
    best_acc = 0
    best_par = [0.2, 0.2, 0.2, 0.2, 0.2]
    delta = 0.05
    for a in range(0, 20):
        for b in range(0, 20 - a):
            for c in range(0, 20 - a - b):
                for d in range(0, 20 - a - b - c):
                    e = 20 - a - b - c - d
                    count += 1
                    if count % 1000 == 0:
                        print count
                    par1 = a * delta
                    par2 = b * delta
                    par3 = c * delta
                    par4 = d * delta
                    par5 = e * delta
                    total_proba = []
                    for i in range(0, prob_1.shape[0]):
                        tot = par1 * prob_1[i] + par2 * prob_2[i] + par3 * prob_3[i] + par4 * prob_4[i] + par5 * prob_5[
                            i]
                        tot = list(tot)
                        total_proba.append(tot.index(max(tot)))
                    y_clean = list([(tr[0] + 1) / 2 for tr in y_test])
                    count_true = np.sum(np.array(total_proba) == np.array(y_clean))
                    acc = float(count_true) / float(len(y_clean))
                    if acc > best_acc:
                        best_acc = acc
                        best_par = [a, b, c, d, e]
                        print best_acc
                        print best_par


def find_best_weights_multiclass(prob_1, prob_2, prob_3, prob_4, y_test):
    count = 0
    best_acc = 0
    best_par = [0.25, 0.25, 0.25, 0.25]
    delta = 0.01
    for a in range(0, 100):
        for b in range(0, 100 - a):
            for c in range(0, 100 - a - b):
                d = 100 - a - b - c
                count += 1
                if count % 1000 == 0:
                    print count
                par1 = a * delta
                par2 = b * delta
                par3 = c * delta
                par4 = d * delta
                total_proba = []
                # print list(prob_5[0]).index(max(list(prob_5[0])))
                # print prob_5[0]
                for i in range(0, prob_1.shape[0]):
                    tot = par1 * prob_1[i] + par2 * prob_2[i] + par3 * prob_3[i] + par4 * prob_4[i]
                    tot = list(tot)
                    total_proba.append(tot.index(max(tot)) + 2)

                y_clean = list([tr[0] for tr in y_test])
                # print y_clean
                # print y_clean[0]
                # print total_proba[0]
                count_true = np.sum(np.array(total_proba) == np.array(y_clean))
                acc = float(count_true) / float(len(y_clean))
                if acc > best_acc:
                    best_acc = acc
                    best_par = [a, b, c, d]
                    print best_acc
                    print best_par

    return best_acc
    # weights = [0,6,2,5,7]


def garbage_collector(X_test, lr_model, svc_d1, y_test):
    real_proba = [float(99 / float(740)), float(641 / float(740))]
    print real_proba
    print svc_d1.predict(X_test) != lr_model.predict(X_test)
    print svc_d1.predict(X_test[107])
    print lr_model.predict(X_test[107])
    print y_test[107]
    print X_test[107]
    print "---------"
    print svc_d1.predict(X_test[122])
    print lr_model.predict(X_test[122])
    print y_test[122]
    print X_test[122]
    print "---------"
    print svc_d1.predict(X_test[156])
    print lr_model.predict(X_test[156])
    print y_test[156]
    print X_test[156]
    # exit()
    print "abcd:" + str(y_test[107])
    print "antarctica:" + str(y_test[122])
    print "hobbit:" + str(y_test[156])

    # if clf != perceptron_model and clf != svc_linear:
    #     tmp = clf.predict_proba(X_test)
    #     pred_proba = np.zeros(2)
    #     for row in tmp:
    #         pred_proba[list(row).index(max(list(row)))] += 1
    #     pred_proba = pred_proba/float(740)
    #     print pred_proba
    #
    #     print scipy.stats.chisquare(pred_proba, real_proba)

    # print("\nAccuracy/Score of : %0.3f " % (clf.score(X_test, y_test.ravel())))
    # print "abcd:" + str(clf.predict(X_test[107]))
    # print "antarctica:" + str(clf.predict(X_test[122]))
    # print "hobbit:" + str(clf.predict(X_test[156]))

    # # deprecated models: svm_poly, svc_linear, perceptron, gnb
    # print_debug("start training svm poly cls...", run_model.__name__)
    # svc_poly = svm.SVC(kernel='poly', degree=3, C=1, cache_size=7000, probability=True, class_weight=balance).fit(
    #     X_train, y_train.ravel())
    # print_debug("start training svm linear cls...", run_model.__name__)
    # svc_linear = svm.LinearSVC(C=2, class_weight=balance).fit(X_train, y_train.ravel())
    # print_debug("start training perceptron cls...", run_model.__name__)
    # perceptron_model = linear_model.Perceptron(class_weight=balance).fit(X_train, y_train.ravel())
    # print_debug("start training gaussian bayes cls...", run_model.__name__)
    # gnb = naive_bayes.GaussianNB().fit(X_train, y_train.ravel())


if __name__ == "__main__":
    create_x_y(raw_csv_file_path)
    print_genres_data()
    print MODEL_FEATURES
