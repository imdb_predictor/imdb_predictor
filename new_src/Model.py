from sklearn import svm, ensemble, tree, neighbors, linear_model
import numpy as np
from sklearn.metrics import classification_report

from Utils import create_x_y, MODEL_FEATURES, print_debug

titles = ['SVM with linear kernel classifier',
          'Random Forest classifier',
          'Decision Tree classifier',
          'K-Nearest Neighbors classifier',
          'Perceptron classifier',
          'Logistic Regression classifier',
          'Gaussian Naive Bayes classifier',
          'SVM with rbf kernel classifier',
          'SVM with polynomial kernel classifier',
          'Linear SVM classifier',
          ]

balance = {1: 6}


def run_model():
    X_train, X_test, y_train, y_test = create_x_y(use_feature_permutations=False)
    print_debug("start training svm deg1 cls...", run_model.__name__)
    svc_d1 = svm.SVC(kernel='linear', C=2, cache_size=7000, probability=True, class_weight=balance).fit(X_train,
                                                                                                        y_train.ravel())
    print_debug("start training svm rbf cls...", run_model.__name__)
    svc_rbf = svm.SVC(kernel='rbf', gamma=0.8, C=2, cache_size=7000, probability=True, class_weight=balance).fit(
        X_train, y_train.ravel())
    print_debug("start training random forest cls...", run_model.__name__)
    rfc = ensemble.RandomForestClassifier(criterion="entropy", class_weight=balance).fit(X_train, y_train.ravel())
    print_debug("start training decision tree cls...", run_model.__name__)
    dtc = tree.DecisionTreeClassifier(criterion="entropy", class_weight=balance).fit(X_train, y_train.ravel())
    print_debug("start training knn cls...", run_model.__name__)
    knc = neighbors.KNeighborsClassifier().fit(X_train, y_train.ravel())
    print_debug("start training logistic regression cls...", run_model.__name__)
    lr_model = linear_model.LogisticRegression(class_weight=balance).fit(X_train, y_train.ravel())
    print_debug("finished training all classifiers...", run_model.__name__)
    for i, clf in enumerate([svc_d1, rfc, dtc, knc, lr_model, svc_rbf]):
        print "-" * 200
        print("\nAccuracy Score (clf.score()) of %s: %0.3f " % (titles[i], clf.score(X_test, y_test.ravel())))
        print('\n' +
              classification_report(y_test.ravel(), clf.predict(X_test),
                                    target_names=['class 0 - bad', 'class 1 - good']))
        print("Classifier information %s\n" % clf)
        if i in [0, 4]:
            feature_weights = [(MODEL_FEATURES[i], round(val, 3)) for i, val in enumerate(clf.coef_[0])]
            sorted_feature_weights = sorted(feature_weights, key=lambda tup: abs(tup[1]), reverse=True)
            print "Top 10 Features Weights: ", sorted_feature_weights[:20]
        elif i in [1, 2]:
            feature_weights = [(MODEL_FEATURES[i], round(val, 3)) for i, val in enumerate(clf.feature_importances_)]
            sorted_feature_weights = sorted(feature_weights, key=lambda tup: abs(tup[1]), reverse=True)
            print "Top 10 Features Weights: ", sorted_feature_weights[:20]


def run_multiclass():
    X_train, X_test, y_train, y_test = create_x_y(binary=False)
    dtc = tree.DecisionTreeClassifier(criterion="entropy").fit(X_train, y_train.ravel())
    nnc = neighbors.KNeighborsClassifier(n_neighbors=50, weights='distance').fit(X_train, y_train.ravel())
    lr_model = linear_model.LogisticRegression(solver='lbfgs', multi_class='multinomial').fit(X_train, y_train.ravel())
    rfc = ensemble.RandomForestClassifier(criterion="entropy").fit(X_train, y_train.ravel())
    # dtc = dtc.predict(X_test)
    clf = lr_model
    feature_weights = [(MODEL_FEATURES[i], round(val, 3)) for i, val in enumerate(clf.coef_[0])]
    sorted_feature_weights = sorted(feature_weights, key=lambda tup: abs(tup[1]), reverse=True)
    print clf
    print "Top 10 Features Weights: ", sorted_feature_weights[:10]
    print lr_model.score(X_test, y_test.ravel())
    for clf in [dtc, rfc]:
        print clf
        feature_weights = [(MODEL_FEATURES[i], round(val, 3)) for i, val in enumerate(clf.feature_importances_)]
        sorted_feature_weights = sorted(feature_weights, key=lambda tup: abs(tup[1]), reverse=True)
        print "Top 10 Features Weights: ", sorted_feature_weights[:10]
        print clf.score(X_test, y_test.ravel())

    print nnc
    print nnc.score(X_test, y_test.ravel())


def run_multi_balance():
    X_train, X_test, y_train, y_test = create_x_y()

    cl_svm = svm.SVC(kernel='linear', C=2, cache_size=2000, probability=True).fit(X_train,
                                                                                  y_train.ravel())  # FIXME class_weight param
    cl_rf = ensemble.RandomForestClassifier(criterion="entropy", ).fit(X_train, y_train.ravel())
    cl_dt = tree.DecisionTreeClassifier(criterion="entropy").fit(X_train, y_train.ravel())
    cl_knn = neighbors.KNeighborsClassifier().fit(X_train, y_train.ravel())
    # cl_perc = linear_model.Perceptron().fit(X_train, y_train.ravel())
    cl_logr = linear_model.LogisticRegression().fit(X_train, y_train.ravel())

    prob_1 = cl_svm.predict_proba(X_test)

    prob_2 = cl_rf.predict_proba(X_test)
    prob_3 = cl_dt.predict_proba(X_test)
    prob_4 = cl_knn.predict_proba(X_test)
    # prob_5 = cl_perc.predict_proba(X_test)
    prob_5 = cl_logr.predict_proba(X_test)
    par_1 = 0.2
    par_2 = 0.2
    par_3 = 0.2
    par_4 = 0.2
    par_5 = 0.2
    total_proba = []
    for i in range(0, prob_1.shape[0]):
        tot = par_1 * prob_1[i] + par_2 * prob_2[i] + par_3 * prob_3[i] + par_4 * prob_4[i] + par_5 * prob_5[i]
        tot = list(tot)
        total_proba.append(tot.index(max(tot)))

    y_clean = list([(tr[0] + 1) / 2 for tr in y_test])
    # print np.array(total_proba) == np.array(y_clean)
    count_true = np.sum(np.array(total_proba) == np.array(y_clean))
    acc = float(count_true) / float(len(y_clean))
    print "accuracy: " + str(acc)

if __name__ == "__main__":
    run_model()
    # run_multiclass()
    # run_multi_balance()
