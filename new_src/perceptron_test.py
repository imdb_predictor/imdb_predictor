from sklearn import linear_model

from Utils import *
# import sklearn.ensemble.RandomForestClassifier
# import sklearn.neighbors.KNeighborsClassifier
# import sklearn.tree.DecisionTreeClassifier

titles = ['Perceptron parameter testing']

ACCEPTED_ACCURACY = 0.76

def runModel1():
    X_train, X_test, y_train, y_test = create_x_y()
    print titles[0]

    penalty_p = ['', 'l2', 'l1', 'elasticnet']
    alpha_p = [0.00001, 0.0001, 0.001, 0.01, 0.1]
    iter_p = [1, 3, 5, 7, 9, 11, 13]

    for pen in penalty_p:
        for alph in alpha_p:
            for i in iter_p:

                if pen != '':
                    percept_model = linear_model.Perceptron(penalty=pen, alpha=alph, n_iter=i).fit(X_train,
                                                                                                   y_train.ravel())
                else:
                    percept_model = linear_model.Perceptron(alpha=alph, n_iter=i).fit(X_train, y_train.ravel())
                score = percept_model.score(X_test, y_test.ravel())
                if score > ACCEPTED_ACCURACY:
                    print("penalty:" + pen + " iterations:" + str(i) + " alpha:" + str(alph))
                    print("Accuracy: %0.2f%% " % (score))

# best result:
# accuracy: 0.76
# penalty=l1, iterations:5, alpha=0.0001


if __name__ == "__main__":
    runModel1()

