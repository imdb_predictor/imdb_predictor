from Utils import sort_csv_by_year, RAW_CSV_FEATURES, create_x_y, MODEL_FEATURES
from sklearn import ensemble

import csv
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
CSV_PATH = "./movie_metadata.csv"

#           'color' 'director_name' 'num_critic_for_reviews' 'duration' 'director_facebook_likes'
#           'actor_3_facebook_likes' 'actor_2_name' 'actor_1_facebook_likes' 'gross' 'genres'
#           'actor_1_name' 'movie_title' 'num_voted_users' 'cast_total_facebook_likes' 'actor_3_name'
#           'facenumber_in_poster' 'plot_keywords' 'movie_imdb_link' 'num_user_for_reviews' 'language'
#           'country' 'content_rating' 'budget' 'title_year' 'actor_2_facebook_likes'
#           'imdb_score' 'aspect_ratio' 'movie_facebook_likes']
#           total 28

# model features:
#           'color', 'duration', 'director_facebook_likes', 'actor_1_facebook_likes', 'actor_2_facebook_likes',
#           'actor_3_facebook_likes', 'language', 'country', 'budget', 'title_year',
#           'is_Sci-Fi_genre', 'is_Crime_genre', 'is_Romance_genre', 'is_Animation_genre', 'is_Music_genre',
#           'is_Comedy_genre', 'is_War_genre', 'is_Horror_genre', 'is_Film-Noir_genre', 'is_Western_genre',
#           'is_News_genre', 'is_Thriller_genre', 'is_Adventure_genre', 'is_Mystery_genre', 'is_Short_genre',
#           'is_Drama_genre', 'is_Action_genre', 'is_Documentary_genre', 'is_Musical_genre', 'is_History_genre',
#           'is_Family_genre', 'is_Fantasy_genre', 'is_Sport_genre', 'is_Biography_genre']
def test_future_movie():
    X_train, X_test, y_train, y_test = create_x_y(csv_file_path="./movie_metadata_copy.csv", scale_data=True)
    # x,y = create_x_y(csv_file_path="./movie_metadata_copy.csv", scale_data=False)
    # print x[x.shape[0]-1]

    # dtc = tree.DecisionTreeClassifier(criterion="entropy", class_weight=balance).fit(X_train, y_train.ravel())
    rfc = ensemble.RandomForestClassifier(criterion="entropy", class_weight={1:6}).fit(X_train, y_train.ravel())
    print rfc.predict(X_test[-1])
    X_train, X_test, y_train, y_test = create_x_y(csv_file_path="./movie_metadata_copy.csv",binary=False)
    rfc = ensemble.RandomForestClassifier(criterion="entropy", class_weight=None).fit(X_train, y_train.ravel())
    print rfc.predict(X_test[-1])
    print X_test.shape


def top_features_check():
    #is_documentary:    print MODEL_FEATURES[27]
    x, y = create_x_y(binary=True, scale_data=False)
    data = np.insert(x, x.shape[1], y.ravel(), axis=1)
    doc_list = data[:, 27]
    print sum(doc_list)
    documentary_movies = data[data[:, 27] == 1]
    ratings_of_documentary = documentary_movies[:, 34]
    print sum(ratings_of_documentary)/float(len(ratings_of_documentary))
    #
    # duration_vs_label = data[:, [1,34]]
    # labels_dict = OrderedDict()
    # for lab in [-1, 1]:
    #     print lab
    #     tmp = duration_vs_label[duration_vs_label[:, 1] == float(lab)]
    #     print tmp
    #     # print nansum(tmp[:,0])
    #     count = tmp.shape[0]
    #     if count!=0:
    #         avg_duration = np.nansum(tmp[:, 0])/float(count)
    #         labels_dict[lab] = avg_duration
    #     else:
    #         labels_dict[lab] = 0
    # # labels_dict = OrderedDict(labels_dict)
    # print labels_dict
    # plot_dict(labels_dict)


    year_vs_label = data[:, [9, 34]]
    print year_vs_label
    labels_dict = OrderedDict()
    # for lab in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
    for lab in [-1, 1]:
        print lab
        tmp = year_vs_label[year_vs_label[:, 1] == float(lab)]
        print tmp
        # print nansum(tmp[:,0])
        count = tmp.shape[0]
        if count != 0:
            avg_duration = np.nansum(tmp[:, 0]) / float(count)
            labels_dict[lab] = avg_duration
        else:
            labels_dict[lab] = 0
    # labels_dict = OrderedDict(labels_dict)
    print labels_dict
    plot_dict(labels_dict)

def data_analysis():
    # row_num, data = sort_csv_by_year(CSV_PATH, csv.DictReader(open(CSV_PATH)))
    raw_data = np.array(list(csv.reader(open(CSV_PATH, "rb"), delimiter=',')))
    titles = raw_data[0]
    data = raw_data[1::]
    ratings_list = list(map(float, data[:, 25]))
    print "Data Analysis:"
    print "number of movies:"+ str(data.shape[0])
    print "average movie rating:"+str(sum(ratings_list)/float(len(ratings_list)))

    all_actors = set(data[:, 10]).union(set(data[:, 6])).union(set(data[:, 14]))
    print "number of unique actors:" + str(len(all_actors))
    all_directors = set(data[:, 1])
    print "number of unique directors:" + str(len(all_directors))

    movies_per_actor = {}
    movies_per_director = {}
    movies_per_country = {}
    movies_per_language = {}
    movies_per_genre = {}
    for row in data:
        if row[6] in movies_per_actor.keys():
            movies_per_actor[row[6]] += 1
        else:
            movies_per_actor[row[6]] = 1

        if row[10] in movies_per_actor.keys():
            movies_per_actor[row[10]] += 1
        else:
            movies_per_actor[row[10]] = 1

        if row[14] in movies_per_actor.keys():
            movies_per_actor[row[14]] += 1
        else:
            movies_per_actor[row[14]] = 1

        if row[1] in movies_per_director.keys():
            movies_per_director[row[1]] += 1
        else:
            movies_per_director[row[1]] = 1

        if row[20] in movies_per_country.keys():
            movies_per_country[row[20]] += 1
        else:
            movies_per_country[row[20]] = 1

        if row[19] in movies_per_language.keys():
            movies_per_language[row[19]] += 1
        else:
            movies_per_language[row[19]] = 1

    movies_per_actor[''] = 0 #clean missing values
    actors_sorted = sorted(movies_per_actor.items(), key=lambda x: x[1], reverse=True)
    directors_sorted = sorted(movies_per_director.items(), key=lambda x: x[1], reverse=True)
    languages_sorted = sorted(movies_per_language.items(), key=lambda x: x[1], reverse=True)
    countries_sorted = sorted(movies_per_country.items(), key=lambda x: x[1], reverse= True)

    print "top 10 actors by number of movies: "+ str(actors_sorted[:10])
    print "top 10 directors by number of movies:" + str(directors_sorted[:10])
    print "top 10 languages by number of movies:" + str(languages_sorted[:10])
    print "top 10 countries by number of movies:" + str(countries_sorted[:10])
    is_movie_good = [x >= 7.5 for x in ratings_list]
    print "total movies above 7.5:" + str(sum(is_movie_good))
    rating_dist = np.zeros(11)
    for mov in ratings_list:
        rating_dist[int(round(mov))] += 1
    print rating_dist
    print str(rating_dist/data.shape[0]*100)
    print rating_dist[10]
    print max(ratings_list)
    print "---"
    print MODEL_FEATURES
    x, y = create_x_y(scale_data=False)
    print "movie #1: " + str(x[0])
    print "movie #2: " + str(x[1])
    # exit()

    plot_dict(dict(actors_sorted[:10]))
    plot_dict(dict(directors_sorted[:8]))
    plot_dict(dict(languages_sorted[:10]))
    plot_dict(dict(countries_sorted[:10]))


def plot_dict(dict_input):
    plt.bar(range(len(dict_input)), dict_input.values(), align='center')
    plt.xticks(range(len(dict_input)), dict_input.keys())
    plt.show()




if __name__ == "__main__":
    # top_features_check()
    # data_analysis()
    test_future_movie()
