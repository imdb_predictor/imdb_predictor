import numpy as np
import csv
# all categories:
# movie_title, color, num_critic_for_reviews, movie_facebook_likes, duration, director_name
# director_facebook_likes, actor_3_name, actor_3_facebook_likes, actor_2_name,actor_2_facebook_likes
# actor_1_name, actor_1_facebook_likes, gross, genres, num_voted_users, cast_total_facebook_likes
# facenumber_in_poster, plot_keywords, movie_imdb_link, num_user_for_reviews, language, country, content_rating
# budget, title_year, imdb_score, aspect_ratio

#10 permanent categories: color, duration, director, actor_3, actor_2, actor_1, language, country, budget, title_year
#extra: genres **, facenumber_in_poster *, plot_keywords *,


def get_column_as_list(fieldname, path="./movie_metadata_sorted.csv"):
    return [row[fieldname] for row in csv.DictReader(open(path))]

##TODO: add int_or_zero to replace int function in map
##TODO: normalizatio of each column

def parse_data(input_file):
    #color:
    color_col = get_column_as_list('color')
    color_col = parse_color(color_col)

    #duration:
    duration_col = get_column_as_list('duration')
    duration_col = map(int, duration_col)

    #director_likes:
    dir_name_col = get_column_as_list('director_name')
    dir_fb_col = get_column_as_list('director_facebook_likes')
    dir_col = parse_director(dir_name_col,dir_fb_col)

    #actors 1-3 likes:
    actor1_name_col = get_column_as_list('actor_1_name')
    actor2_name_col = get_column_as_list('actor_3_name')
    actor3_name_col = get_column_as_list('actor_3_name')
    actor1_fb_col = get_column_as_list('actor_1_facebook_likes')
    actor2_fb_col = get_column_as_list('actor_2_facebook_likes')
    actor3_fb_col = get_column_as_list('actor_2_facebook_likes')
    actor1_col,actor2_col,actor3_col = parse_actors(actor1_name_col,actor2_name_col,actor3_name_col,actor1_fb_col,actor2_fb_col,actor3_fb_col)

    #language:
    language_col = get_column_as_list('language')
    language_col = parse_language(language_col)

    # country:
    country_col = get_column_as_list('country')
    country_col = parse_country(country_col)

    # budget:
    budget_col = get_column_as_list('budget')
    budget_col = map(int,budget_col)
    # title_year:
    year_col = get_column_as_list('title_year')
    year_col = map(int, year_col)

    # genre:
    genre_col = get_column_as_list('genres')
    genre_col = parse_genre(genre_col)

    #num_of_rows = len(input_file)

    num_of_cols = 10+genre_col.shape[1]

    X = budget_col
    Y = get_column_as_list('imdb_score')
    Y = map(int, Y)
    # Y = map(float, Y) #FIXME check

    return X,Y


def parse_color(col):
    new_col = list()
    for line in col:
        if str(line).lower() == "color" or str(line).lower() == "":
            new_col.append(1)
        else:
            new_col.append(0)
    return new_col

#likes_only - change to false to include director names
def parse_director(dir_name_col, dir_fb_like_col,likes_only=True):
    if likes_only:
        return dir_fb_like_col
    else:
        return dir_fb_like_col  # TODO: need to expand and make a director index?

def parse_actors(actor1_names, actor2_names, actor3_names, actor1_fb, actor2_fb, actor3_fb, actor_names_only=True):
    if actor_names_only:
        return actor1_fb,actor2_fb,actor3_fb
    else:
        return actor1_fb, actor2_fb, actor3_fb

def parse_genre(genre_col):
    size = len(genre_col)
    all_genres = get_all_genres(genre_col)
    sorted_genres = list(all_genres)
    num_of_genres = len(sorted_genres)
    sorted_genres.sort()

    new_col = np.zeros((size, num_of_genres))
    index = 0
    for single_val in genre_col:
        for single_genre in single_val.split("|"):
            genre_index = sorted_genres.index(single_genre)
            new_col[index][genre_index] = 1
        index += 1
    return new_col

def parse_language(col):
    size = len(col)
    new_col = np.zeros(size)
    index = 0
    for single_val in col:
        is_english = False
        if single_val == "English":
            is_english = True
        new_col[index] = is_english
        index += 1
    return new_col

def parse_country(col):
    size = len(col)
    new_col = np.zeros(size)
    index = 0
    for single_val in col:
        is_usa = False
        if single_val == "USA":
            is_usa = True
        new_col[index] = is_usa
        index += 1
    return new_col

def get_all_genres(col):
    all_genres = set()
    for item in col:
        movie_genres = item.split("|")
        for genre in movie_genres:
            if not genre in all_genres:
                all_genres.add(genre)
    return all_genres


if __name__== "__main__":
    print "hi"

